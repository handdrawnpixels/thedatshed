        <!--location starts-->
          <div class="location">
        <div class="container">
          <div class="cnt-blk">
            <h2>Say hello</h2>
            <div class="hello pull-left">
              <?php the_field('left_text_area', 'option'); ?>
              </p>
            </div>
            <div class="details pull-right">
              <div class="number">
                <a href="#"><?php the_field('phone_number', 'option'); ?></a>
              </div>
              <div class="mail">
                <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
              </div>
              <div class="map">
                <p><?php the_field('address', 'option'); ?></p>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="navicon">
              <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
          <!--location ends-->
    </div>
    <!-- maincontent ends -->

    <!-- footer starts -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="footer_in clearfix">
                    <p class="col-sm-9 col-xs-12"><?php the_field('footer_legals', 'option'); ?> . <a href="<?php echo site_url(); ?>/privacy-policy">Privacy Policy</a> | <a href="<?php echo site_url(); ?>/cookies-policy">Cookies Policy</a> | <a href="<?php echo site_url(); ?>/terms-and-conditions">Terms and Conditions</a></p>
                    <div class="ftr_socl_icons col-sm-3 col-xs-12">
                        <ul class="clearfix">
                            <li><a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="<?php the_field('git_link', 'option'); ?>" target="_blank"><i class="fa fa-github-alt" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer ends -->
            
    
</div>
<!-- wrapper ends -->