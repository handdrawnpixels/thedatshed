
    <!-- maincontent Starts -->
    <div class="main_content">    
      <!--banner Starts-->
      <div class="banner">
	      
	      						<?php $pagebanner = get_field('page_banner'); ?>

        <figure>
          <img src="<?php echo $pagebanner['sizes']['home-banner']; ?>"  alt="<?php echo $pagebanner['alt'] ?>" class="img-responsive">
        </figure>
                                                        <?php get_template_part('include/nav'); ?>
      </div>
      <!--banner ends-->
      
      
      
      

	        		                        	<?php

if ( 'post' == get_post_type() ) { ?>
<div class="review-section">
        <div class="container">
          <div class="row">
            <div class="review-main inner-container">
              <div class="review">
						<div class="filter_info_top ehgt col-sm-6 col-md-12">

                                        <span class="con_person">
                                            <img src="<?php bloginfo('template_directory'); ?>/images/person.png" alt="image" width="21" height="20">
                                            <small><?php the_author_posts_link(); ?></small>
                                        </span>
                                        <span class="con_date">
                                            <img src="<?php bloginfo('template_directory'); ?>/images/calender.png" alt="image" width="21" height="21">
                                            <small><?php echo get_the_date(); ?></small>
                                        </span>
                                    </div>                                        
            </div>
              
            </div>
          </div>
        </div>
      </div>
                <?php } elseif ( 'lacasitaleeds' == get_post_type() ) { ?>

      
           <?php }
	

?>

      
      
      
      <?php while(has_sub_field("page_builder")): // Flexible Content Builder ?>
	 
	 	<?php if(get_row_layout() == "large_image_area"): // Header Block 1 ?>	
						<?php $image = get_sub_field('image'); ?>

      <!--banner Starts-->
      <div class="banner">
        <figure>
          <img src="<?php echo $image['url']; ?>"  alt="<?php echo $image['alt'] ?>" class="img-responsive">
        </figure>
      </div>
      <!--banner ends-->


		<?php elseif(get_row_layout() == "text_area"): // Image Block ?>
		
				
      <!--review-section starts-->
      <div class="review-section">
        <div class="container">
          <div class="row">
            <div class="review-main inner-container">
              <div class="review">
                <h1><?php the_sub_field('title'); ?></h1>
               <?php the_sub_field('text_area'); ?>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <!--review-section ends-->
      
      
      
		<?php elseif(get_row_layout() == "quote"): // Image Block ?>
		
				
      <!--review-section starts-->
      <div class="review-section">
        <div class="container">
          <div class="row">
            <div class="review-main inner-container">
              
              <div class="parcles">
                <h2><?php the_sub_field('quote'); ?></h2>
             </div>
            </div>
          </div>
        </div>
      </div>
      <!--review-section ends-->




		<?php elseif(get_row_layout() == "grey_area"): // Content ?>	
			

<!--The Data Refinery-->

         <div class="data_refinery_blk">
            <div class="container">
                <div class="row">
                    <div class="data_refinery">
                        <div class="ven_daigram">
	                        						<?php $medimage = get_sub_field('image'); ?>

					              <figure>
									          <img src="<?php echo $medimage['sizes']['medium-image']; ?>" alt="<?php echo $medimage['alt'] ?>" class="img-responsive">
							  	</figure>                       
						</div>
                        <h2><?php the_sub_field('title'); ?></h2>
                        <?php the_sub_field('text_area'); ?>
                    </div>
                </div>
            </div>
        </div>

            <!--The Data Refinery ENDS-->

		
		<?php elseif(get_row_layout() == "purple_image_area_with_text"): // Content ?>	
			
	                        						<?php $purpleimage = get_sub_field('image_background'); ?>

      <!--banner1 starts-->
      <div class="banner1">
        <figure>
           <img src="<?php echo $purpleimage ['sizes']['home-banner']; ?>" alt="<?php echo $purpleimage['alt'] ?>"  class="img-responsive">
          <div class="period">
            <div class="container">
              <div class="row">
                <div class="inner-container">
	                <?php if( get_sub_field('title') ): ?>
	                                <h2><?php the_sub_field('title'); ?></h2>
					<?php endif; ?>

                 <?php the_sub_field('text_area'); ?>
                </div>
              </div>
            </div>
          </div>
        </figure>
      </div>
      <!--banner1 ends-->
	  		
	  		<?php elseif(get_row_layout() == "3_columns"): // Content ?>	

      <!--proposals starts-->
      <div class="proposals  no-gutters">
        <div class="container">
          <div class="row">
            <div class="inner-container">
              <div class="tech-main text-center">
                      <?php if( have_rows('column') ): ?>

     <ul class=" clearfix text-center">

	<?php while( have_rows('column') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$content = get_sub_field('text_area');

		?>

        <li class="col-sm-4 col-xs-12">

              <div class="technology">

				<figure><img src="<?php echo $image['sizes']['3-col-icon']; ?>" alt="<?php echo $image['alt'] ?>" width="67" height="67"/></figure>


		    <?php echo $content; ?>
             </div>

		</li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>
	                            </div>
            </div>
          </div>
        </div>
      </div>
      <!--proposals ends-->


	  		<?php elseif(get_row_layout() == "2_columns"): // Content ?>	


<?php 		$imageright = get_sub_field('column_2'); ?>
 <!--dashboard starts-->
      <div class="dashboard">
        <div class="container">
          <div class="row">
            <div class="inner-container clearfix">
                <div class="cnt-lft col-sm-6">
                         <?php the_sub_field('column_1'); ?>

               
                    
                    <?php if( get_sub_field('button_text') ): ?>
                       <div class="sign">
	                             <a class="sign" href="<?php the_sub_field('link'); ?>"> <?php the_sub_field('button_text'); ?></a>
	                                  </div>
					<?php endif; ?>
                    
                    
             
                </div>
              <div class="mobile_img col-sm-6">
                <div class="cnt-rgt">
                  <img src="<?php echo $imageright['sizes']['2-col-img']; ?>" width="335" height="394" alt="<?php echo $imageright['alt'] ?>"  class="img-responsive">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--dashboard ends-->
	  		
	  		<?php elseif(get_row_layout() == "icon_and_text"): // Content ?>	

								
<!--groups starts-->
      <div class="groups no-gutters">
        <div class="container">
          <div class="row">
            <div class="inner-container">
	            
	            
                <div class="tech-main tech_icon">  
	                
	                <?php if( have_rows('column') ): ?>

     <ul class=" clearfix text-center">

	<?php while( have_rows('column') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$content = get_sub_field('text_area');

		?>

        <li class="col-sm-4 col-xs-12">

              <div class="technology">

				<figure><img src="<?php echo $image['sizes']['3-col-icon']; ?>" alt="<?php echo $image['alt'] ?>" /></figure>


		    <?php echo $content; ?>
             </div>

		</li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>
	                
	                
	             
                </div>
            </div>
          </div>
        </div>
      </div>
      <!--groups ends-->

				
		

		

            		<?php elseif(get_row_layout() == "case_studies"): // Content ?>	

						<?php 

$posts = get_sub_field('select_linked_case_study');
if( $posts ): ?>
 <!--case starts-->
      <div class="case  no-gutters">
        <div class="container-fluid">
          <div class="row">
            <h2>Case Studies</h2>
     <ul class=" clearfix">
	<?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
<?php $casestudyimage = get_field('thumbnail', $p->ID); ?>

	    
			  <li class="col-sm-4 col-xs-12">
                <div class="finanical">
                 <a href="<?php echo get_permalink( $p->ID ); ?>"> <img src="<?php echo $casestudyimage ['sizes']['case-study']; ?>" width="342" height="341" alt="<?php echo $casestudyimage['alt'] ?>"></a>
                  <div class="airline ehgt">
                   <a href="<?php echo get_permalink( $p->ID ); ?>"><h3><?php the_field('client_name', $p->ID); ?></h3>
                    <strong><?php the_field('sub_title', $p->ID); ?></strong></a>
                  </div>
                </div>
              </li>
              
              
	<?php endforeach; ?>
	</ul>
	   </div>
        </div>
      </div>
      <!--case ends-->

<?php endif; ?>
       
				
				<?php endif; ?>
		

	 
	<?php endwhile; // End Flexible Field ?>
	
















