            <div class="menu_sec">
                <div class="menu_sec_in">
                    <nav>
                
                    
<?php $args = array(
				'theme_location' => 'header-menu',
                'container'       => '', 
                'container_class' => 'false', 
                'container_id'    => '',
                'menu_class'      => '', 
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'items_wrap'      => '<ul>%3$s</ul>',
                'depth'           => 0,
      ); ?>
      <?php wp_nav_menu( $args ); ?>
  </nav>

                    <div class="social_icons">
                        <ul class="clearfix">
                            <li><a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="<?php the_field('git_link', 'option'); ?>" target="_blank"><i class="fa fa-github-alt" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>    
            </div>
            
                                      