<?php
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

add_theme_support( 'post-thumbnails' );
add_image_size( 'home-banner', 1700, 478, true);
add_image_size( 'page-banner', 1024, 478, true);
add_image_size( 'medium-image', 500, 350, true);
add_image_size( 'case-study', 342, 341, true);
add_image_size( 'overview-page', 479, 232, true);
add_image_size( '3-col-icon', 70, 60, true);
add_image_size( '2-col-img', 335, 400, true);
add_image_size( 'logo', 184, 145, true);


// Move Yoast to bottom
function yoasttobottom() {
        return 'low';
}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Datashed Site Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

################################################################################
//  HDP Wordpress Footer
################################################################################
function remove_footer_admin () {
  echo 'Fuelled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Developed by <a href="http://www.handdrawnpixels.com" target="_blank">Hand Drawn Pixels</a></p>';
}

add_filter('admin_footer_text', 'remove_footer_admin');


################################################################################
//   Wordpress Log In 
################################################################################
function custom_login() { 
echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/css/custom-login.css" />'; 
}
add_action('login_head', 'custom_login');

################################################################################
// Change the Login Logo URL
################################################################################
add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
	return 'http://www.thedatashed.co.uk/';
}


################################################################################
// Remove Admin Bar Elements
################################################################################

function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    // $wp_admin_bar->remove_menu('view-site'); 
}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );

function cc_mime_types($mimes) {
 $mimes['svg'] = 'image/svg+xml';
 return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
