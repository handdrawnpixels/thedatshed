<?php 	/*
		Template Name: Case Study


		*/
		get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <!-- maincontent Starts -->
    <div class="main_content">    
      <!--banner Starts-->
      <div class="banner">
    
                                                        <?php get_template_part('include/nav'); ?>
      </div>
      <!--banner ends-->
      

        <div class="container">
            <div class="row">
                <div class="filtering">
                    <div class="filtering_tab">
                        <span>Filter</span>
                        <h3 class="filter_label">Filter</h3>
                        <ul class="filter_nav">
                            <li><a href="#" data-filter="*">All</a></li>
                                            
								 
								 <?php 
									 
									 $args = array(
    'taxonomy'      => 'case-study-category',
    'parent'        => 0, // get top level categories
    'orderby'       => 'name',
    'order'         => 'ASC',
    'hierarchical'  => 1,
    'pad_counts'    => 0
);

$categories = get_categories( $args );

foreach ( $categories as $category ){

    echo '<li><a href="#" data-filter=".'. $category->slug . '">'. $category->name . '</a></li>';

    
} ?>

                        </ul>
                    </div>    
                    
                    <div class="filter_data">
                        <ul class="clearfix grid">
	                        
	                        
	                        <?php
$_terms = get_terms( array('case-study-category') );

foreach ($_terms as $term) :

    $term_slug = $term->slug;
    $_posts = new WP_Query( array(
                'post_type'         => 'case-study',
                'posts_per_page'    => 10, //important for a PHP memory limit warning
                'tax_query' => array(
                    array(
                        'taxonomy' => 'case-study-category',
                        'field'    => 'slug',
                        'terms'    => $term_slug,
                    ),
                ),
            ));

    if( $_posts->have_posts() ) :

        while ( $_posts->have_posts() ) : $_posts->the_post(); $count++; 
        
 $image = get_field('thumbnail');

        ?>
               <li class="col-md-6 col-sm-12 <?php echo $term->slug ?>">
                                <div class="filter_info clearfix">
                                    <figure class="col-sm-6 col-md-12">
										<img src="<?php echo $image['sizes']['overview-page']; ?>" alt="image">
										<span class="pu-over-lay"></span>
										<div class="read_more">
                                            <a href="<?php the_permalink() ?>"><img src="<?php bloginfo('template_directory'); ?>/images/icon1.png" alt="icon"><small>Read more</small></a>
                                        </div>
									</figure>
                                    <div class="filter_info_btm ehgt col-sm-6 col-md-12">
                                        <h2><?php the_title() ?></h2>
                                        <p><?php the_field('sub_title') ?></p>
                                        
                                    </div>
                                </div>
                            </li>

        <?php
        endwhile;

    endif;
    wp_reset_postdata();

endforeach;
?>

	                    
                        </ul>
                    </div>
                </div>
                <!-- <div class="pagenation">
	                
	                
                    <ul class="clearfix">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                    </ul>
                </div> -->
            </div>    
        </div>
<script src="http://kenwheeler.github.io/slick/slick/slick.js"></script>   

<script>
    $(window).load(function(){
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
    $(document).ready(function() {
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
</script>    
<script>
	$(document).ready(function() {
	$('.filter_label').click(function(e){
		$(this).closest('.filtering_tab').toggleClass('open_select');
		});
	
	$('.filter_nav li a').click(function(e){
		
		var txt = $(this).text();
		$(this).closest('.filtering_tab').removeClass('open_select');
		$(this).closest('.filtering_tab').find('.filter_label').text(txt);
		
		
		});
        
        

	});
</script>

<?php endwhile; endif; ?>

<?php get_footer('casestudy'); ?>