<?php 	/*
		Template Name: Blog


		*/
		get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <!-- maincontent Starts -->
    <div class="main_content">    
      <!--banner Starts-->
      <div class="banner">
    
                                                        <?php get_template_part('include/nav'); ?>
      </div>
      <!--banner ends-->
      

        <div class="container">
            <div class="row">
                <div class="filtering">
                    <div class="filtering_tab">
                        <span>Filter</span>
                        <h3 class="filter_label">Filter</h3>
                        <ul class="filter_nav">
                            <li><a href="#" data-filter="*">All</a></li>
                                            
								 
								 <?php 
									 
									 $args = array(
    'taxonomy'      => 'category',
    'parent'        => 0, // get top level categories
    'orderby'       => 'name',
    'order'         => 'ASC',
    'hierarchical'  => 1,
    'pad_counts'    => 0
);

$categories = get_categories( $args );

foreach ( $categories as $category ){

    echo '<li><a href="#" data-filter=".'. $category->slug . '">'. $category->name . '</a></li>';

    
} ?>

                        </ul>
                    </div>    
                    
                    <div class="filter_data">
                        <ul class="clearfix grid">
	                        
	                      <!-- query -->
<?php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $query = new WP_Query( array(
                'post_type'         => 'post',
        'posts_per_page' => 20,
        'paged' => $paged
    ) );
    
?>

<?php if ( $query->have_posts() ) : ?>

<!-- begin loop -->
<?php while ( $query->have_posts() ) : $query->the_post(); 
	
	 $image = get_field('news_image');
$categories = get_the_category();
$cls = '';

if ( ! empty( $categories ) ) {
  foreach ( $categories as $cat ) {
    $cls .= $cat->slug . ' ';
  }
}
?>

<li class="col-md-6 col-sm-12 <?php echo $cls; ?>">
                                <div class="filter_info clearfix">
                                    <figure class="col-sm-6 col-md-12">
										<img src="<?php echo $image['sizes']['overview-page']; ?>" alt="image">
										<span class="pu-over-lay"></span>
										<div class="read_more">
                                            <a href="<?php the_permalink() ?>"><img src="<?php bloginfo('template_directory'); ?>/images/icon1.png" alt="icon"><small>Read more</small></a>
                                        </div>
									</figure>
                                    <div class="filter_info_btm ehgt col-sm-6 col-md-12">
                                        <h2><?php the_title() ?></h2>
                                        <p><?php the_field('snippet') ?></p>

                                        <span class="con_person">
                                            <img src="<?php bloginfo('template_directory'); ?>/images/person.png" alt="image" width="21" height="20">
                                            <small><?php the_author_posts_link(); ?></small>
                                        </span>
                                        <span class="con_date">
                                            <img src="<?php bloginfo('template_directory'); ?>/images/calender.png" alt="image" width="21" height="21">
                                            <small><?php echo get_the_date(); ?></small>
                                        </span>
                                    </div>
                                </div>
                            </li>

<?php endwhile; ?>
<!-- end loop -->
     					</ul>
                    </div>
                </div>
<div class="pagenation">
	 <ul class="clearfix">
		 
<?php 
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<li><i></i> %1$s</li>', __( '<', 'text-domain' ) ),
            'next_text'    => sprintf( '<li>%1$s <i></i></li>', __( '>', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
            'before_page_number' => '<li>',
	'after_page_number'  => '</li>',
        ) );
    ?>
    
	 </ul>
</div>
<?php wp_reset_postdata(); ?>

<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
	                                </div>    
        </div>
<script src="http://kenwheeler.github.io/slick/slick/slick.js"></script>   

<script>
    $(window).load(function(){
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
    $(document).ready(function() {
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
</script>    
<script>
	$(document).ready(function() {
	$('.filter_label').click(function(e){
		$(this).closest('.filtering_tab').toggleClass('open_select');
		});
	
	$('.filter_nav li a').click(function(e){
		
		var txt = $(this).text();
		$(this).closest('.filtering_tab').removeClass('open_select');
		$(this).closest('.filtering_tab').find('.filter_label').text(txt);
		
		
		});
        
        

	});
</script>

<?php endwhile; endif; ?>

<?php get_footer('casestudy'); ?>