<?php get_header(); ?>

    <!-- maincontent Starts -->
    <div class="main_content">    
      <!--banner Starts-->
      <div class="banner">
    
                                                        <?php get_template_part('include/nav'); ?>
      </div>
      <!--banner ends-->
      

        <div class="container">
            <div class="row">
                <div class="filtering">
                    <div class="filtering_tab">
                        <h3><?php _e( 'Author', 'blankslate' ); ?>: <?php the_author_link(); ?></h3>
                        

                    </div>    
                    
                    <div class="filter_data">
                        <ul class="clearfix grid">
	                        
<?php while ( have_posts() ) : the_post(); 
	
	 $image = get_field('news_image');
$categories = get_the_category();
$cls = '';

if ( ! empty( $categories ) ) {
  foreach ( $categories as $cat ) {
    $cls .= $cat->slug . ' ';
  }
}

	
?>
<li class="col-md-6 col-sm-12 <?php echo $cls; ?>">
                                <div class="filter_info clearfix">
                                    <figure class="col-sm-6 col-md-12">
										<img src="<?php echo $image['sizes']['overview-page']; ?>" alt="image">
										<span class="pu-over-lay"></span>
										<div class="read_more">
                                            <a href="<?php the_permalink() ?>"><img src="<?php bloginfo('template_directory'); ?>/images/icon1.png" alt="icon"><small>Read more</small></a>
                                        </div>
									</figure>
                                    <div class="filter_info_btm ehgt col-sm-6 col-md-12">
                                        <h2><?php the_title() ?></h2>
                                        <p><?php the_field('snippet') ?></p>

                                        <span class="con_person">
                                            <img src="<?php bloginfo('template_directory'); ?>/images/person.png" alt="image" width="21" height="20">
                                            <small><?php the_author_posts_link(); ?></small>
                                        </span>
                                        <span class="con_date">
                                            <img src="<?php bloginfo('template_directory'); ?>/images/calender.png" alt="image" width="21" height="21">
                                            <small><?php echo get_the_date(); ?></small>
                                        </span>
                                    </div>
                                </div>
                            </li>

<?php endwhile; ?>
<!-- end loop -->
     					</ul>
                    </div>
                </div>
<div class="pagenation">
	 <ul class="clearfix">
		 
<?php 
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<li><i></i> %1$s</li>', __( '<', 'text-domain' ) ),
            'next_text'    => sprintf( '<li>%1$s <i></i></li>', __( '>', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
            'before_page_number' => '<li>',
	'after_page_number'  => '</li>',
        ) );
    ?>
    
	 </ul>
</div>
<?php wp_reset_postdata(); ?>

	                                </div>    
        </div>
<script src="http://kenwheeler.github.io/slick/slick/slick.js"></script>   

<script>
    $(window).load(function(){
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
    $(document).ready(function() {
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
</script>    
<script>
	$(document).ready(function() {
	$('.filter_label').click(function(e){
		$(this).closest('.filtering_tab').toggleClass('open_select');
		});
	
	$('.filter_nav li a').click(function(e){
		
		var txt = $(this).text();
		$(this).closest('.filtering_tab').removeClass('open_select');
		$(this).closest('.filtering_tab').find('.filter_label').text(txt);
		
		
		});
        
        

	});
</script>



<?php get_footer(); ?>