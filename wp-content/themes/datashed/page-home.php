<?php 	/*
		Template Name: Home


		*/
		get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <!-- maincontent Starts -->
    <div class="main_content">
        <div class="banner">
	            
	            
	            
	            
	            
	            
	            <?php if( have_rows('slider') ): ?>

	<ul  class="banner_slide">

	<?php while( have_rows('slider') ): the_row(); 

		// vars
		$image = get_sub_field('image');


		?>

		<li>


				<figure><img src="<?php echo $image['sizes']['home-banner']; ?>" alt="<?php echo $image['alt'] ?>" width="1024" height="471"/></figure>

                   <div class="banner_cnt">
                        <div class="container">
                            <div class="row">
                                <section class="banner_cnt_in">
                                    <a href="#" class="logohiden"><img src="<?php bloginfo('template_directory'); ?>/images/logo_icon.png" alt="icon" width="105" height="145"></a>
                                    
                                    <div class="loader-s"></div>

                                    <h1>We love data</h1>
                                </section>
                           </div>     
                        </div>
                    </div>
		</li>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>

                                                <?php get_template_part('include/nav'); ?>


        </div>
        <div class="product_blk">
            <div class="container">
                <div class="row">
                    <div class="product">
               <?php the_field('text_under_slider') ?>
                                       
                       
                              <?php if( get_field('button_content') ): ?>
	                             <a class="sign" href="<?php the_field('button_link'); ?>"> <button name="name" type="button" class="see_btn"><?php the_field('button_content'); ?></button></a>

					<?php endif; ?>
                    
                    
                    </div>
                </div>    
            </div>
        </div>
        <div class="people_blk">
            <div class="container">
                <div class="row">
                    <section class="people_cnt_in">
                        <h2><?php the_field('title'); ?></h2>
                       <?php the_field('text_area'); ?>
                       
                          <?php if( get_field('button_content_2') ): ?>
	                             <a class="sign" href="<?php the_field('button_link_2'); ?>"> <button name="name" type="button" class="see_btn get"><?php the_field('button_content_2'); ?></button></a>

					<?php endif; ?>
                    
                                            
                        <ul class="clearfix">
                            <li class="col-sm-4">
                                <div class="circle_blk">
                                    <figure><img src="<?php the_field('infographic_image_1'); ?>" alt="circle" width="172" height="172"></figure>
                                </div>
                            </li>
                            <li class="col-sm-4">
                                <div class="circle_blk">
                                    <figure><img src="<?php the_field('infographic_image_2'); ?>" alt="circle" width="172" height="172"></figure>
                                </div>
                            </li>
                            <li class="col-sm-4">
                                <div class="circle_blk">
                                    <figure><img src="<?php the_field('infographic_image_3'); ?>" alt="circle" width="172" height="172"></figure>
                                </div>
                            </li>
                        </ul>
                    </section>
                </div>    
            </div>
        </div>
        <div class="clients_blk">
            <div class="container">
                <div class="row">
                    <section class="clients_blk_in">
                        <h2>Clients</h2>
                        <ul class="clearfix">
	                        
	                        
	                        <?php $loop = new WP_Query( array( 'post_type' => 'case-study', 'posts_per_page' => 100 ) ); ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); 
	
	 $logo = get_field('logo');
	 $sub_title = get_field('sub_title');

	
?>

 <li>
                                <div class="clients_logo">
	                                                                        <?php if( get_field('sub_title') ): ?>

                                    <a href="<?php the_permalink() ?>">			
	                                    					<?php endif; ?>
							
	                                    <img src="<?php echo $logo['sizes']['logo']; ?>" alt="image">
	                                                                        <?php if( get_field('sub_title') ): ?>

                                    </a>
                                    	                                    					<?php endif; ?>

                                        <?php if( get_field('sub_title') ): ?>
	                     <div class="study"> 
                                        <a href="<?php the_permalink() ?>"> <span><small><img src="<?php bloginfo('template_directory'); ?>/images/icon1.png" alt="icon"></small> Read case study</span></a>
                                    </div>
					<?php endif; ?>
					
                                    
                                </div>
                            </li>



<?php endwhile; ?>

                        </ul>
                    </section>
                </div>
            </div>
        </div>
        <div class="news_blk">
            <div class="container">
                <div class="row">
                    <section class="news_blk_in">
                        <h2>News & Views</h2>
                        <ul class="clearfix">
	                        
	                        
	                        <?php $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 2 ) ); ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); 
	
	 $image = get_field('news_image');

	
?>


 <li class="col-sm-6">
                               <div class="news_cnt ehgt">
                                    <div class="news_img"><figure><img src="<?php echo $image['sizes']['overview-page']; ?>" alt="img" width="325" height="190"> </figure>
										<span class="pu-over-lay"></span>
								   		<div class="read_more">
                                            <a href="<?php the_permalink() ?>"><img src="<?php bloginfo('template_directory'); ?>/images/icon1.png" alt="icon"><small>Read more</small></a>
                                        </div> 
								   </div>
                                    <h3><?php the_title() ?></h3>
                                    <p><?php the_field('snippet') ?></p>
                                    <ul class="clearfix profile">
                                        <li><?php the_author_posts_link(); ?></li>
                                        <li><a href="#" class="calender"><?php echo get_the_date(); ?></a></li>
                                    </ul>
                                </div>
								
                            </li>

<?php endwhile; ?>




                        </ul>
                    </section>
                </div>
            </div>
        </div>


<?php endwhile; endif; ?>

<?php get_footer(); ?>