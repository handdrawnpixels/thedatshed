$(document).ready(function(e) {
    // FOR Menu //
    $('.menu_btn').click(function(){
        $('body').toggleClass('open_menu');
        $('.menu_sec').slideToggle();
    });
    // FOR Slider //
    $('.banner_slide').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
    });
    
    
	equalheight = function(container){
        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {
      
           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;
      
           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
        }
        $(window).load(function() {  equalheight('.ehgt'); });
        $(window).resize(function(){  equalheight('.ehgt'); });
        $(window).width(function(){  equalheight('.ehgt'); });
	
});