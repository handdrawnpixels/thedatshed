                                                <?php get_template_part('include/address-area'); ?>


<!-- SET: SCRIPTS -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
<script type="text/javascript" src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>  

<script>
    $(window).load(function(){
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
    $(document).ready(function() {
        var $grid = $('.grid').isotope({});
		
		$(document).on( 'click', '.filter_nav li a', function() {	
			  var filterValue = $(this).attr('data-filter');
			  $grid.isotope({ filter: filterValue });
		});
    });
</script>    
<script>
	$(document).ready(function() {
	$('.filter_label').click(function(e){
		$(this).closest('.filtering_tab').toggleClass('open_select');
		});
	
	$('.filter_nav li a').click(function(e){
		
		var txt = $(this).text();
		$(this).closest('.filtering_tab').removeClass('open_select');
		$(this).closest('.filtering_tab').find('.filter_label').text(txt);
		
		
		});
        
        
        
// FOR Equalheight //
	equalheight = function(container){
	var currentTallest = 0,
		 currentRowStart = 0,
		 rowDivs = new Array(),
		 $el,
		 topPosition = 0;
	 $(container).each(function() {
	
	   $el = $(this);
	   $($el).height('auto')
	   topPostion = $el.position().top;
	
	   if (currentRowStart != topPostion) {
		 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		   rowDivs[currentDiv].height(currentTallest);
		 }
		 rowDivs.length = 0; // empty the array
		 currentRowStart = topPostion;
		 currentTallest = $el.height();
		 rowDivs.push($el);
	   } else {
		 rowDivs.push($el);
		 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		 rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}
	
	$(window).load(function() {  equalheight('.ehgt'); });  
	$(window).resize(function(){  equalheight('.ehgt'); });
	$(window).width(function(){  equalheight('.ehgt'); });
	});
</script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/custom_script.js"></script>    

<?php wp_footer(); ?>

</body>
</html>